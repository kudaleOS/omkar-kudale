from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Inches,Pt,RGBColor,Cm

def document1():
    #Document No.1 as normal document

    document = Document()

    #head1
    head11 = input("ENTER HEADING - 1 : ")
    head1 = document.add_heading(head11)
    para11 = input("WRITE CONTENT FOR PARAGRAPH - 1 : ")
    para1 = document.add_paragraph(para11)

    para22 = input("WRITE CONTENT FOR PARAGRAPH - 2 : ")
    para2 = document.add_paragraph(para22)

    #formatting para1
    para1.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para1.paragraph_format.first_line_indent = Inches(1)
    para1.paragraph_format.line_spacing = 1

    ht = input("ENTER CONTENT TO HIGHLIGHT : ")
    run = document.add_paragraph("").add_run(ht)
    font = run.font
    font.color.rgb = RGBColor(0xff, 0x99, 0xcc)
    font.name = 'Impact'
    font.size = Pt(11)


    #formatting para2
    para2.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para2.paragraph_format.first_line_indent = Inches(1)
    para2.paragraph_format.line_spacing = 1

    #head2
    head22 = input("ENTER HEADING - 2 : ")
    head2 = document.add_heading(head22,level=2)

    para33 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para3 = document.add_paragraph(para33)

    para44 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para4 = document.add_paragraph(para44)

    #formatting para3
    para3.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para3.paragraph_format.first_line_indent = Inches(1)
    para3.paragraph_format.line_spacing = 1
    #formatting para4
    para4.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para4.paragraph_format.first_line_indent = Inches(1)
    para4.paragraph_format.line_spacing = 1

    #head2
    head33 = input("ENTER HEADING : ")
    head3 = document.add_heading(head33,level=2)
    para55 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para5 = document.add_paragraph(para55)
    para66 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para6 = document.add_paragraph(para66)
    #formatting para5
    para5.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para5.paragraph_format.first_line_indent = Inches(1)
    para5.paragraph_format.line_spacing = 1
    #formatting para6
    para6.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para6.paragraph_format.first_line_indent = Inches(1)
    para6.paragraph_format.line_spacing = 1

    img = document.add_picture("t1.png",width=Inches(2), height=Inches(1.33))
    #width=docx.shared.Inches(1),height=docx.shared.Cm(4))
    last_paragraph = document.paragraphs[-1] 
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

    document.save("doc1.docx")
    print("DOCUMENT IS SAVED AS 'doc1.docx'")


def document2():
    #Document No.2 as a report
    document1 = Document()
    img1 = document1.add_picture("t1.png",width=Inches(4), height=Inches(6.87))
    last_paragraph1 = document1.paragraphs[-1] 
    last_paragraph1.alignment = WD_ALIGN_PARAGRAPH.CENTER

    HEAD21 = input("ENTER REPORT TITLE : ")
    head21 = document1.add_heading(HEAD21,level=2)
    last_paragraph1 = document1.paragraphs[-1] 
    last_paragraph1.alignment = WD_ALIGN_PARAGRAPH.CENTER

    PARA11 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para11 = document1.add_paragraph(PARA11)
    PARA12 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para12 = document1.add_paragraph(PARA12)

    para11.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para11.paragraph_format.first_line_indent = Inches(1)
    para11.paragraph_format.line_spacing = 1

    para12.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para12.paragraph_format.first_line_indent = Inches(1)
    para12.paragraph_format.line_spacing = 1

    HEAD11 = input("ENTER HEADING : ")
    head11 = document1.add_heading(HEAD11)
    PARA13 = input("WRITE CONTENT TO HIGHLIGHT : ")
    para13 = document1.add_paragraph("").add_run(PARA13)
    font = para13.font
    font.color.rgb = RGBColor(153, 0, 0)
    font.name = 'Arial'
    font.size = Pt(11)
    para13.style = 'Emphasis'
    PARA14 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para14 = document1.add_paragraph(PARA14)

    HEAD22 = input("ENTER HEADING : ")
    head22 = document1.add_heading(HEAD22,level=2)
    PARA15 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para15 = document1.add_paragraph(PARA15)
    PARA16 = input("WRITE CONTENT FOR PARAGRAPH : ")
    para16 = document1.add_paragraph(PARA16)

    document1.save("doc2.docx")
    print("DOCUMENT IS SAVED AS 'doc2.docx'")


def document3():
    #Document No.3 as a letter
    document2 = Document()
    PARA21 = input("ENTER YOUR NAME : ")
    para21 = document2.add_paragraph(PARA21)
    PARA22 = input("ENTER STREET ADDRESS : ")
    para22 = document2.add_paragraph(PARA22)
    PARA23 = input("CITY, ST ZIP CODE : ")
    para23 = document2.add_paragraph(PARA23)
    PARA233 = input("ENTER DATE : ")
    para233 = document2.add_paragraph(PARA233)
    para2333 = document2.add_paragraph("")
    para23333 = document2.add_paragraph("")

    PARA24 = input("ENTER RECIPIENT NAME : ")
    para24 = document2.add_paragraph(PARA24)
    PARA25 = input("ENTER TITLE : ")
    para25 = document2.add_paragraph(PARA25)
    PARA26 = input("ENTER COMPANY NAME : ")
    para26 = document2.add_paragraph(PARA26)
    PARA27 = input("STREET ADDRESS : ")
    para27 = document2.add_paragraph(PARA27)
    PARA28 = input("CITY, ST ZIP CODE : ")
    para28 = document2.add_paragraph(PARA28)
    para221 = document2.add_paragraph("")

    #head1
    HEAD21 = input("ENTER HEADING : ")
    head21 = document2.add_heading(HEAD21)
    PARA29 = input("ENTER PARAGRAPH : ")
    para29 = document2.add_paragraph(PARA29)
    PARA210 = input("ENTER PARAGRAPH : ")
    para210 = document2.add_paragraph(PARA210)

    para29.paragraph_format.first_line_indent = Inches(1)
    para210.paragraph_format.first_line_indent = Inches(1)
    para29.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para210.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    #head2
    HEAD22 = input("ENTER HEADING : ")
    head22 = document2.add_heading(HEAD22,level=2)
    PARA211 = input("ENTER PARAGRAPH : ")
    para211 = document2.add_paragraph(PARA211)
    PARA212 = input("ENTER PARAGRAPH : ")
    para212 = document2.add_paragraph(PARA212)

    para211.paragraph_format.first_line_indent = Inches(1)
    para212.paragraph_format.first_line_indent = Inches(1)
    para211.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para212.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    #head2
    HEAD22 = input("ENTER HEADING : ")
    head22 = document2.add_heading(HEAD22,level=2)
    PARA213 = input("ENTER PARAGRAPH : ")
    para213 = document2.add_paragraph(PARA213)
    PARA214 = input("ENTER PARAGRAPH : ")
    para214 = document2.add_paragraph(PARA213)

    para213.paragraph_format.first_line_indent = Inches(1)
    para214.paragraph_format.first_line_indent = Inches(1)
    para213.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para214.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    para215 = document2.add_paragraph("")
    para216 = document2.add_paragraph("")

    para217 = document2.add_paragraph("Sincerely,")
    para218 = document2.add_paragraph("")
    para219 = document2.add_paragraph("")
    PARA220 = input("ENTER YOUR NAME : ")
    para220 = document2.add_paragraph(PARA220)

    img3 = document2.add_picture("t1.png",width=Inches(0.82), height=Inches(0.83))

    document2.save("doc3.docx")
    print("DONE.")
    print("DOCUMENT IS SAVED AS 'doc3.docx'")


def selectFormat(choice): 
    switcher = { 
        1: document1, 
        2: document2, 
        3: document3, 
    } 
    #return switcher.get(choice, "INVALID SELECTIION.")
    func=switcher.get(choice,lambda :'Invalid')
    return func()

if __name__ == "__main__":
    print("1.NORMAL FORMAT  2.REPORT FORMAT  3.LETTER FORMAT")
    choice = int(input("SELECT DOCUMENT FORMAT : "))
    selectFormat(choice)
    
