from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Inches,Pt,RGBColor,Cm

#Document No.1
document = Document()

#head1
head1 = document.add_heading("python-docx 0.8.10 : Heading -1")
para1 = document.add_paragraph("python-docx is a Python library for creating and updating Microsoft Word (.docx) files.More information is available in the python-docx documentation. Migrate Document.numbering_part to DocumentPart.numbering_part. The numbering_part property is not part of the published API and is an interim internal feature to be replaced in a future release, perhaps with something like Document.numbering_definitions. In the meantime, it can now be accessed using Document.part.numbering_part.")
para2 = document.add_paragraph("Paragraph.style now returns a Style object. Previously it returned the style name as a string. The name can now be retrieved using the Style.name property, for example, paragraph.style.name. Refactor docx.Document from an object into a factory function for new docx.document.Document object. Extract methods from prior docx.Document and docx.parts.document.DocumentPart to form the new API class and retire docx.Document class.")
#formatting para1
para1.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para1.paragraph_format.first_line_indent = Inches(1)
para1.paragraph_format.line_spacing = 1

run = document.add_paragraph("python-docx is a Python library for creating and updating Microsoft Word (.docx) files.More information is available.").add_run(" Python library for creating and updating Microsoft Word.")
font = run.font
font.color.rgb = RGBColor(0xff, 0x99, 0xcc)
font.name = 'Impact'
font.size = Pt(11)


#formatting para2
para2.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para2.paragraph_format.first_line_indent = Inches(1)
para2.paragraph_format.line_spacing = 1

#head2
head2 = document.add_heading("Python-list : Heading 2",level=2)
para3 = document.add_paragraph("In Python, list is a type of container in Data Structures, which is used to store multiple data at the same time. Unlike Sets, the list in Python are ordered and have a definite count. The elements in a list are indexed according to a definite sequence and the indexing of a list is done with 0 being the first index.")
para4 = document.add_paragraph("In Python programming, a list is created by placing all the items (elements) inside a square bracket [ ], separated by commas. It can have any number of items and they may be of different types (integer, float, string etc.). Also, a list can even have another list as an item")
#formatting para3
para3.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para3.paragraph_format.first_line_indent = Inches(1)
para3.paragraph_format.line_spacing = 1
#formatting para4
para4.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para4.paragraph_format.first_line_indent = Inches(1)
para4.paragraph_format.line_spacing = 1

#head2
head3 = document.add_heading("Python-string : Heading -2",level=2)
para5 = document.add_paragraph("Python String. In Python, Strings are arrays of bytes representing Unicode characters. However, Python does not have a character data type, a single character is simply a string with a length of 1. Square brackets can be used to access elements of the string")
para6 = document.add_paragraph("To create a string, put the sequence of characters inside either single quotes, double quotes, or triple quotes and then assign it to a variable. You can look into how variables work in Python in the Python variables tutorial. For example, you can assign a character 'a' to a variable single_quote_character .")
#formatting para5
para5.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para5.paragraph_format.first_line_indent = Inches(1)
para5.paragraph_format.line_spacing = 1
#formatting para6
para6.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para6.paragraph_format.first_line_indent = Inches(1)
para6.paragraph_format.line_spacing = 1

img = document.add_picture("t1.png",width=Inches(2), height=Inches(1.33))
#width=docx.shared.Inches(1),height=docx.shared.Cm(4))
last_paragraph = document.paragraphs[-1] 
last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

document.save("t1.docx")
print("1st DOCUMENT IS SAVED AS 't1.docx'")


#Document No.2
document1 = Document()
img1 = document1.add_picture("t1.png",width=Inches(4), height=Inches(6.87))
last_paragraph1 = document1.paragraphs[-1] 
last_paragraph1.alignment = WD_ALIGN_PARAGRAPH.CENTER

head21 = document1.add_heading("Python-REPORT",level=2)
last_paragraph1 = document1.paragraphs[-1] 
last_paragraph1.alignment = WD_ALIGN_PARAGRAPH.CENTER

para11 = document1.add_paragraph("If you use Microsoft Word (or a similar word processor), you probably know well enough how to save a document. You click Save, choose a folder, give the document a name, and then click Save, OK, or whatever.")
para12 = document1.add_paragraph("By default, Microsoft Word uses its own, proprietary document format. In the old days, that was the Doc format, but as of Word 2007 (and continuining with Word 2010), it's Docx.")

para11.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para11.paragraph_format.first_line_indent = Inches(1)
para11.paragraph_format.line_spacing = 1

para12.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para12.paragraph_format.first_line_indent = Inches(1)
para12.paragraph_format.line_spacing = 1

head11 = document1.add_heading("HEADING-1")
para13 = document1.add_paragraph("").add_run("First check out a few tips to help you quickly format your report. You might be amazed at how easy it is.")
font = para13.font
font.color.rgb = RGBColor(153, 0, 0)
font.name = 'Arial'
font.size = Pt(11)
para13.style = 'Emphasis'
para14 = document1.add_paragraph("To replace the placeholder text on this page, you can just select it all and then start typing. But don’t do that just yet!")

head22 = document1.add_heading("HEADING-2",level=2)
para15 = document1.add_paragraph("You might like the photo on the cover page as much as we do, but if it’s not ideal for your report, it’s easy to replace it with your own.")
para16 = document1.add_paragraph("Just delete the placeholder picture. Then, on the Insert tab, click Picture to select one from your files.")

document1.save("t2.docx")
print("2nd DOCUMENT IS SAVED AS 't2.docx'")


#Document No.3
document2 = Document()
para21 = document2.add_paragraph("Your Name ")
para22 = document2.add_paragraph("Street Address")
para23 = document2.add_paragraph("City, ST ZIP Code")
para23 = document2.add_paragraph("Date")
para23 = document2.add_paragraph("")
para23 = document2.add_paragraph("")

para24 = document2.add_paragraph("Recipient Name")
para25 = document2.add_paragraph("Title")
para26 = document2.add_paragraph("Company Name")
para27 = document2.add_paragraph("Street Address")
para28 = document2.add_paragraph("City, ST ZIP Code")
para221 = document2.add_paragraph("")

#head1
head21 = document2.add_heading("Heading -1")
para29 = document2.add_paragraph("I am currently researching positions in the field of Industry or job field and Name of person referring you suggested that you would be an excellent source of information. I would like to learn more about the types of jobs available in this field and the skills required for them.")
para210 = document2.add_paragraph("As you will see from the enclosed resume, my education and work experience are in job field. I hope to transfer the skills that I’ve acquired over the years to a job in Industry or job field.")

para29.paragraph_format.first_line_indent = Inches(1)
para210.paragraph_format.first_line_indent = Inches(1)
para29.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para210.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
#head2
head22 = document2.add_heading("Heading 2",level=2)
para211 = document2.add_paragraph("I hope that you can find 30 minutes to meet with me before the end of the month. I will contact you the week of date to set up an appointment. ")
para212 = document2.add_paragraph("If you have questions, please contact me by phone at phone or by email at email. I appreciate your time in considering my request.")

para211.paragraph_format.first_line_indent = Inches(1)
para212.paragraph_format.first_line_indent = Inches(1)
para211.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para212.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
#head2
head22 = document2.add_heading("Heading 2",level=2)
para213 = document2.add_paragraph("By default, Microsoft Word uses its own, proprietary document format. In the old days, that was the Doc format, but as of Word 2007 (and continuining with Word 2010), it's Docx.")
para214 = document2.add_paragraph("If you use Microsoft Word (or a similar word processor), you probably know well enough how to save a document. You click Save, choose a folder, give the document a name, and then click Save, OK, or whatever.")

para213.paragraph_format.first_line_indent = Inches(1)
para214.paragraph_format.first_line_indent = Inches(1)
para213.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para214.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
para215 = document2.add_paragraph("")
para216 = document2.add_paragraph("")

para217 = document2.add_paragraph("Sincerely,")
para218 = document2.add_paragraph("")
para219 = document2.add_paragraph("")
para220 = document2.add_paragraph("Your Name")

img3 = document2.add_picture("t1.png",width=Inches(0.82), height=Inches(0.83))

document2.save("t3.docx")
print("3rd DOCUMENT IS SAVED AS 't3.docx'")







